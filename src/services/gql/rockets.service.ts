import { getRocketById, getRockets } from '../../providers/gql/rockets.provider'
import { useRocketsStore } from '../../store/gql/rockets.store'

export const loadRockets = async () => {
    const rockets = await getRockets()
    if (rockets.length === 0) {
        console.warn('No rockets were found')
        return
    }

    const store = useRocketsStore()
    store.rockets = rockets
}

export const loadRocketById = async (rocketId: string) => {
    const rocket = await getRocketById(rocketId)
    if (!rocket) {
        console.warn(`No rocket with id ${rocketId} was found`)
        return
    }
    const store = useRocketsStore()
    store.selectedRocket = rocket
}

export default { loadRocketById, loadRockets }
