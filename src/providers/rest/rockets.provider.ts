import axios from 'axios'
import { Rocket } from '../../models/rocket.model'

export const getRockets = async () => {
    return await axios
        .get<Rocket[]>('https://api.spacex.land/rest/rockets')
        .then((response) => {
            const { data } = response
            return data
        })
        .catch((error) => {
            console.error(`There was an error when getting Rockets ${error}`)
            return [] as Rocket[]
        })
}

export const getRocketById = async (rocketId: string) => {
    return await axios
        .get<Rocket>(`https://api.spacex.land/rest/rocket/${rocketId}`)
        .then((response) => {
            const { data } = response
            return data
        })
        .catch((error) => {
            console.error(`There was an error when getting rocket with id ${rocketId} ${error}`)
            return {} as Rocket
        })
}
