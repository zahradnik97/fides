/* import GetRocketByIdGql from '../../apollo/query/GetRocketById.gql'
import GetRocketsGql from '../../apollo/query/GetRockets.gql' */
import gql from 'graphql-tag'
import { Rocket } from '../../models/rocket.model'
import { apolloClient } from '../../plugins/apolloClient'

export const getRockets = async () => {
    return await apolloClient
        .query<{ rockets: Rocket[] }>({
            // this should be in gql file but vite 3.0 does not have proper loader for these files, at least I can not find yet
            query: gql`
                query getRockets {
                    rockets {
                        id
                        name
                        description
                    }
                }
            `,
        })
        .then((response) => {
            return response.data.rockets
        })
        .catch((error) => {
            console.error(`There was an error when getting rockets ${error}`)
            return [] as Rocket[]
        })
}

export const getRocketById = async (rocketId: string) => {
    return await apolloClient
        .query<{ rocket: Rocket }>({
            query: gql`
                query getRocketById($id: ID!) {
                    rocket(id: $id) {
                        id
                        name
                        description
                    }
                }
            `,
            variables: { id: rocketId },
        })
        .then((response) => {
            return response.data.rocket
        })
        .catch((error) => {
            console.error(`There was an error when getting rocket with id ${rocketId} ${error}`)
            return {} as Rocket
        })
}
