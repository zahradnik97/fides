import axios from 'axios'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import VueAxios from 'vue-axios'
import App from './App.vue'
import './style.css'

const pinia = createPinia()
const app = createApp(App)
app.use(pinia)
app.use(VueAxios, axios)
app.mount('#app')
