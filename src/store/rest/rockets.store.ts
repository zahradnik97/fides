import { defineStore } from 'pinia'
import { reactive, Ref, ref } from 'vue'
import { Rocket } from '../../models/rocket.model'

export const useRocketsStore = defineStore('rocketsrest', () => {
    const rockets: Ref<Rocket[]> = ref([])
    const selectedRocket: Rocket = reactive({})

    return { rockets, selectedRocket }
})

export default { useRocketsStore }
